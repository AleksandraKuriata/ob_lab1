#Aleksandra Kuriata

function KahanFloat16()
  return Float16(3.0) * (Float16(4.0) / Float16(3.0) - Float16(1.0)) - Float16(1.0)
end
println(KahanFloat16())
println(eps(Float16))

function KahanFloat32()
  return Float32(3.0) * (Float32(4.0) / Float32(3.0) - Float32(1.0)) - Float32(1.0)
end
println(KahanFloat32())
println(eps(Float32))

function KahanFloat64()
  return Float64(3.0) * (Float64(4.0) / Float64(3.0) - Float64(1.0)) - Float64(1.0)
end
println(KahanFloat64())
println(eps(Float64))
