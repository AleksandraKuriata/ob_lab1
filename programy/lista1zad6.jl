#Aleksandra Kuriata

function firstAlgorithm(x)
  return sqrt(x^2.0 + 1.0) - 1.0
end

function secondAlgorithm(x)
  return x^2 / (sqrt(x^2.0 + 1.0) + 1.0)
end

function checkAlgorithms()
  for i = 1 : 10
    x = 8.0^(-i)
    println("\n iteration = $(i)")
    println("--------------------------------------------------------")
    println("First algorithm  : $(firstAlgorithm(x))")
    println("Second algorithm : $(secondAlgorithm(x))")
   end
end
checkAlgorithms()
