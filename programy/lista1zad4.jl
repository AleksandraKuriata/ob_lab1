#Aleksandra Kuriata

function findValueBiggerThanOne()
  x = Float64(1.0)
  while Float64( x * (Float64(1.0) / x)) == 1.0
    x = x + eps(Float64)
  end
  println(x)
  return x
end
findValueBiggerThanOne()
