#Aleksandra Kuriata

function machepsFloat16()
  x = Float16(1.0)
  macheps = Float16(1.0)
  while x + (macheps / Float16(2.0)) != x
      macheps = macheps / Float16(2.0)
  end
  return macheps
end
println(machepsFloat16())
println(eps(Float16))

function machepsFloat32()
  x = Float32(1.0)
  macheps = Float32(1.0)
  while x + (macheps / Float32(2.0)) != x
      macheps = macheps / Float32(2.0)
  end
  return macheps
end
println(machepsFloat32())
println(eps(Float32))

function machepsFloat64()
  x = Float64(1.0)
  macheps = Float64(1.0)
  while x + (macheps / Float64(2.0)) != x
      macheps = macheps / Float64(2.0)
  end
  return macheps
end

println(machepsFloat64())
println(eps(Float64))
