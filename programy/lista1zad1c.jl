#Aleksandra Kuriata

function maxFloat16()
  maxPower2 = Float16(1.0)
    while !isinf(maxPower2 * Float16(2.0)) #check value is not  infinite
      maxPower2 = maxPower2 * Float16(2.0)
    end
  maxFloat16 = maxPower2
  i = 0.0
    while i <= 52.0
      if !isinf(Float16(maxFloat16 + maxPower2)) #check value is not infinite
        maxFloat16 = maxFloat16 + Float16(maxPower2)
      end
      maxPower2 = maxPower2 / 2.0
    i =  i + 1.0
  end
  return maxFloat16
end
println(maxFloat16())
println(realmax(Float16))

function maxFloat32()
  maxPower2 = Float32(1.0)
    while !isinf(maxPower2 * Float32(2.0))
      maxPower2 = maxPower2 * Float32(2.0)
    end
  maxFloat32 = maxPower2
  i = 0.0
    while i <= 52.0
      if !isinf(maxFloat32 + maxPower2)
        maxFloat32 = maxFloat32 + Float32(maxPower2)
      end
      maxPower2 = maxPower2 / Float32(2.0)
    i = i + 1.0
  end
  return maxFloat32
end
println(maxFloat32())
println(realmax(Float32))

function maxFloat64()
  maxPower2 = Float64(1.0)
    while !isinf(maxPower2 * Float64(2.0))
      maxPower2 = maxPower2 * Float64(2.0)
    end
  maxFloat64 = maxPower2
  i = 0.0
    while i <= 52.0
      if !isinf(maxFloat64 + maxPower2)
        maxFloat64 = maxFloat64 +Float64(maxPower2)
      end
      maxPower2 = maxPower2 / Float64(2.0)
    i = i + 1.0
  end
  return maxFloat64
end
println(maxFloat64())
println(realmax(Float64))
