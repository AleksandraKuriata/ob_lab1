#Aleksandra Kuriata

function distanceValues()
  println("binary representation 1+macheps: $(bits(Float64(1.0)+eps(Float64)))")
  println("binary representation 0.5      : $(bits(Float64(0.5)))")
  println("binary representation 1        : $(bits(Float64(1.0)))")

  println("binary representation 2+1*eps  : $(bits(Float64(2.0)+Float64(1)*eps(Float64)))")
  println("binary representation     2    : $(bits(Float64(2.0)))")
  println("binary representation     4    : $(bits(Float64(4.0)))")
end
distanceValues()
