#Aleksandra Kuriata

function etaFloat16()
  eta16 = Float16(1.0)
  while (eta16 / Float16(2.0)) > 0.0
    eta16 = eta16 / Float16(2.0)
  end
  return eta16
end
println(etaFloat16())
println(nextfloat(Float16(0.0)))

function etaFloat32()
  eta32 = Float32(1.0)
  while (eta32 / Float32(2.0)) > 0.0
    eta32 = eta32 / Float32(2.0)
  end
  return eta32
end
println(etaFloat32())
println(nextfloat(Float32(0.0)))

function etaFloat64()
  eta64 = Float64(1.0)
  while (eta64 / Float64(2.0)) > 0.0
    eta64 = eta64 / Float64(2.0)
  end
  return eta64
end
println(etaFloat64())
println(nextfloat(Float64(0.0)))
